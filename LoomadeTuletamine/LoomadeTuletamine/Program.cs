﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoomadeTuletamine
{
    class Program
    {
        static void Main(string[] args)
        {
            //var c = new Loom("krokodill");
            //var t = new Loom();
            //Console.WriteLine(t);
            var k = new Koduloom("Paula", "kilpkonn");
            Console.WriteLine(k);
            k.Teehäält();

            var m = new Kass("Mooritz", "siiami");
            Console.WriteLine(m);
            m.Silita();
            m.SikutaSabast();
            m.Teehäält();

            var mKoer = new Koer("Taflik");
            var vKoer = new Koer("Spageta", "valge");
            Console.WriteLine(mKoer);
            Console.WriteLine(vKoer);

            mKoer.Teehäält();

            Sepik s = new Sepik();
            Lõuna(vKoer);

            SortedSet<Koer> koerad = new SortedSet<Koer>();
            koerad.Add(mKoer);
            koerad.Add(vKoer);
            koerad.Add(new Koer("crocodile", "roheline"));
            foreach (var x in koerad) Console.WriteLine(x);

            


        }

        public static  void Lõuna(object söök)
        {
            if (söök is ISöödav ahaa)
            ahaa.Süüakse();
            else Console.WriteLine("täna jääme nälga");
        }

    }

    abstract class Loom
    {
        public string Liik;
        public Loom(string liik) => Liik = liik;
        public Loom() : this("tundmatu") { }
        public override string ToString() => $"loom liigist {Liik}";

        public abstract void Teehäält();
        //{
        //    Console.WriteLine($"{this} teeb koledat häält");
        //}
    }

    class Koduloom : Loom
    {
        public string Nimi;
        public Koduloom(string nimi, string liik)
            : base(liik)
        =>
            Nimi = nimi;

        public override void Teehäält()
        {
            Console.WriteLine($"{Nimi} häälitseb");
        }

        public override string ToString() => $"{Liik} {Nimi}";

    }

    class Kass : Koduloom  
    {
        public string Tõug = "segavereline";
        bool Tuju = false;

        public Kass(string nimi, string tõug = "segavereline")
            : base(nimi, "kass")
            => Tõug = tõug;

        public void Silita() => Tuju = true;
        public void SikutaSabast() => Tuju = false;

        public override void Teehäält()
            => Console.WriteLine($"{Nimi} {(Tuju ? "nurrub" : "kräunub")}");

        public override string ToString()
            => $"{Tõug} kiisu {Nimi}";
    }
    class Koer : Koduloom, ISöödav, IComparable
    {
        public string Värv;
        public Koer(string nimi, string värv = "must")
            : base(nimi, "koer")
            => Värv = värv;

        public override string ToString()
            => $"{Värv} koer {Nimi}";

        public override void Teehäält()
            => Console.WriteLine($"{Nimi} haugub");

        public void Süüakse()
        {
            Console.WriteLine($"kutsa {Nimi} pistetakse nahka");
        }

        public int CompareTo(object obj)
        {
            return obj is Koer k ? this.Nimi.CompareTo(k.Nimi) : -1;
        }
    }

    public interface ISöödav
    {
        void Süüakse();
    }

    class Sepik : ISöödav
    {
        public void Süüakse()
        {
            Console.WriteLine("keegi nosib sepikut");
        }
    }

}

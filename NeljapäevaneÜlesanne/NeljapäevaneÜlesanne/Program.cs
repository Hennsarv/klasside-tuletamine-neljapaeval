﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeljapäevaneÜlesanne
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene.New("35503070211", "Henn");
            Õpilane.New("35503070212", "Henn Sarv");
            Õpetaja.New("35503070213", "henn sarv", "Matemaatika");
            Õpilane.New("50507030214", "Juulipoiss");
            Õpilane.New("50508280212", "Augustipoiss");
            Õpilane.New("50509030212", "Septembripoiss");

            foreach (var x in Inimene.Inimesed) Console.WriteLine($"{x} (vanus: {x.Vanus})");

            SortedSet<Inimene> nimekiri = new SortedSet<Inimene>(Inimene.Inimesed);

            Console.WriteLine();

            foreach (var x in nimekiri) Console.WriteLine(x);
            {

            }

        }
    }

    class Inimene : IComparable
    {

        protected static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        public static IEnumerable<Inimene> Inimesed = _Inimesed.Values;

        string _Isikukood; public string Isikukood => _Isikukood;
        string _Nimi;
        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = Proper(value);
        }

        protected Inimene(string isikukood, string nimi)
        {
            _Isikukood = isikukood; Nimi = nimi;
            _Inimesed.Add(isikukood, this);
        }

        public static Inimene New(string isikukood, string nimi)
        {
            if (_Inimesed.ContainsKey(isikukood)) throw new Exception($"{_Inimesed[isikukood].GetType().Name} iskukoodiga {isikukood} on juba olemas");
            return new Inimene(isikukood, nimi);
        }

        public DateTime Sünniaeg
         => new DateTime(
            int.Parse(Isikukood.Substring(1,2)) + 
                (18 + (Isikukood[0]-'1')/2)*100,    // aasta
            int.Parse(Isikukood.Substring(3,2)),    // kuu
            int.Parse(Isikukood.Substring(5,2))    // päev
            );
        

        public virtual int Vanus

        // appi kuidas vanust arvutada
        // leiame isikukoodist sünnikuupäeva (kuidas?)
        // ja siis võrdleme seda tänasega
        // oot aga teeme sünnikuupäeva eraldi propertyna - ehk läheb ka mujal vaja
        {
            get
            {
                int vanus = DateTime.Now.Year - Sünniaeg.Year;
                if (Sünniaeg.AddYears(vanus) > DateTime.Now) vanus--;
                return vanus;
            }
        }
        


        public override string ToString()
        => $"{GetType().Name} {Nimi}";

        public override bool Equals(object obj)
        {
            return obj is Inimene i ? this.Isikukood.Equals(i.Isikukood) : false;
        }

        public static string Proper(string text)
        => string.Join(" ",
                text.Split(' ')
                .Select( x => (x.Substring(0,1).ToUpper() + (x+" ").Substring(1).ToLower()).Trim())
            );

        public int CompareTo(object obj)
        {
            // 0, 1, -1 - kas sama, suurem või väiksem
            int vastus = 0;
            if (obj is Inimene i)
            {
                vastus = this.Vanus.CompareTo(i.Vanus);
                if (vastus == 0) vastus = this.Nimi.CompareTo(i.Nimi);
               // see rida tagab, et kaks inimest ei ole kunagi võrdsed (compare to mõttes)
                if (vastus == 0) vastus = -1;
            }
            else vastus = -1;
            return vastus;

        }
    }

    sealed class Õpilane : Inimene
    {
        string _Klass; public string Klass => _Klass;

        private Õpilane(string isikukood, string nimi, string klass = "eelklass")
            : base(isikukood, nimi) => _Klass = klass;

        public static Õpilane New(string isikukood, string nimi, string klass = "eelklass")
        {
            if (_Inimesed.ContainsKey(isikukood)) throw new Exception($"{_Inimesed[isikukood].GetType().Name} iskukoodiga {isikukood} on juba olemas");
            return new Õpilane(isikukood, nimi, klass);
        }

        public override string ToString()
        => $"{Klass} klassi õpilane {Nimi}";

        

        public override int Vanus
        // inimese vanusel me võtsime aluseks TÄNASE
        // õpilase vanusel me võtame aluseks 1. septembri sel aastal
        {
            get
            {
                DateTime sept = new DateTime(DateTime.Now.Year, 9, 1);
                // DateTime sept = DateTime.Parse("1.9");
                int vanus = sept.Year - Sünniaeg.Year;
                if (Sünniaeg.AddYears(vanus) > sept) vanus--;
                return vanus;
            }
        }

    }

    class Õpetaja : Inimene
    {
        string _Õppeaine; public string Õppeaine => _Õppeaine;
        string _Klass; public string Klass => _Klass;

        private Õpetaja(string isikukood, string nimi, string õppeaine = "üldained")
            : base(isikukood, nimi) => _Õppeaine = õppeaine;

        public static Õpetaja New(string isikukood, string nimi, string õppeaine = "üldained")
        {
            if (_Inimesed.ContainsKey(isikukood)) throw new Exception($"{_Inimesed[isikukood].GetType().Name} iskukoodiga {isikukood} on juba olemas");
            return new Õpetaja(isikukood, nimi, õppeaine);
        }

        // siia võiks hiljem lisada kontrolli, kas selline klass on olemas
        // või muud - mine tea, mis see Henn jälle välja mõtleb
        public void KlassiJuhataja(string klass) => _Klass = klass;

        public override string ToString()
        => $"{Õppeaine} õpetaja {Nimi}";
    }

}

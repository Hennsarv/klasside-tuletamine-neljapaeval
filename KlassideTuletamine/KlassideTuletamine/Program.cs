﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Õpilane ants = new Õpilane { Nimi = "Ants", Isikukood = "50001019999", KlassKusõpib = "3A" };
            Õpetaja viiu = new Õpetaja { Nimi = "Viiu Viinamägi", Isikukood = "47707077777", AineMidaÕpetab = "Geograafia" };
            // TODO: siia lisame hiljem midagi veel
            List<Inimene> inimesed = new List<Inimene>();
            inimesed.Add(ants);
            inimesed.Add(viiu);
            inimesed.Add(new Inimene { Nimi = "Henn", Isikukood = "35503070211" });
            foreach (var i in Inimene.Inimesed) Console.WriteLine(i);
            inimesed.Add(new Õpetaja { Nimi = "Henn", AineMidaÕpetab = "Matemaatika" });
            inimesed.Add(new Õpilane { Nimi = "Henn", KlassKusõpib = "1d" });


            SortedSet<Inimene> nimekiri = new SortedSet<Inimene>(inimesed);
            foreach (var i in nimekiri) Console.WriteLine(i);            
            

        }

        static void TrükiInimene(Inimene x)
        {
            Console.WriteLine($"Meil on {x.GetType().Name} {x.Nimi}");
            if (x is Õpilane õ) Console.WriteLine($"ta õpib {õ.KlassKusõpib} klassis" );
            
            if (x is Õpetaja õp) Console.WriteLine($"ta õpetab ainet {õp.AineMidaÕpetab}");
        }
    }

    class Inimene : IComparable
    {
        public static List<Inimene> Inimesed = new List<Inimene>();
        public string Isikukood;
        public string Nimi;

        public Inimene()
        {
            Inimesed.Add(this);
        }

        public int CompareTo(object obj)
        {
            int vastus =  (obj is Inimene i ? this.Nimi.CompareTo(i.Nimi) : -1) ;
            if (vastus == 0) vastus = this.GetType().Name.CompareTo(obj.GetType().Name);
            return vastus;
        }

        public override string ToString() => $"Inimene nimega {Nimi}";
        
        
    }

    class Õpilane : Inimene
    {
        public string KlassKusõpib;
        public override string ToString() => 
            $"{KlassKusõpib} klassi õpilane {Nimi}";

        public Õpilane() { }  //TODO: uurida, mis jama ma olen segaduses :O
        
    }

    class Õpetaja : Inimene
    {
        public string AineMidaÕpetab;
        public override string ToString() =>
            $"{AineMidaÕpetab} õetaja {Nimi}";
            
        
    }
}
